# Succulents

## Architecture

### Docker Images

#### Mongo

Download the docker image :

    - 
run it with the following command :

    - 

#### Flask Framework

    - Clone the Template

#### Prometheus container & config

Download the docker image :

    - 
run it with the following command :

    - 

#### Grafana container & config

Download the docker image :

    - 
run it with the following command :

    - 

## Design

This design is for entries of succulent plant information to be used in future business. This application will help with the keeping of the plants information

## Code 

To be done 
## Application

This would be my template to create CRUD application:
  - [C]reate
  - [R]ead
  - [U]pddate
  - [D]elete

There is also an upload of an images for each plant 

to run the appplication from docker image :
    - 

from command line :
    - python suc_app.py

to access the app :
    - http://<ip>:8100
## Dependencies

### IAMI


**pip modules :**

ansible==4.0.0
  - ansible-core [required: >=2.11.0,<2.12, installed: 2.11.1]
    - cryptography [required: Any, installed: 3.4.7]
      - cffi [required: >=1.12, installed: 1.14.5]
        - pycparser [required: Any, installed: 2.20]
    - jinja2 [required: Any, installed: 3.0.1]
      - MarkupSafe [required: >=2.0, installed: 2.0.1]
    - packaging [required: Any, installed: 20.9]
      - pyparsing [required: >=2.0.2, installed: 2.4.7]
    - PyYAML [required: Any, installed: 5.4.1]
    - resolvelib [required: >=0.5.3,<0.6.0, installed: 0.5.4]
appdirs==1.4.4
autopep8==1.5.7
  - pycodestyle [required: >=2.7.0, installed: 2.7.0]
  - toml [required: Any, installed: 0.10.2]
confluent-kafka==1.7.0
Flask-WTF==0.15.1
  - Flask [required: Any, installed: 2.0.1]
    - click [required: >=7.1.2, installed: 8.0.1]
      - importlib-metadata [required: Any, installed: 4.5.0]
        - typing-extensions [required: >=3.6.4, installed: 3.10.0.0]
        - zipp [required: >=0.5, installed: 3.4.1]
    - itsdangerous [required: >=2.0, installed: 2.0.1]
    - Jinja2 [required: >=3.0, installed: 3.0.1]
      - MarkupSafe [required: >=2.0, installed: 2.0.1]
    - Werkzeug [required: >=2.0, installed: 2.0.1]
      - dataclasses [required: Any, installed: 0.8]
  - itsdangerous [required: Any, installed: 2.0.1]
  - WTForms [required: Any, installed: 2.3.3]
    - MarkupSafe [required: Any, installed: 2.0.1]
ibmsecurity==2021.5.10.1
  - requests [required: Any, installed: 2.25.1]
    - certifi [required: >=2017.4.17, installed: 2021.5.30]
    - chardet [required: >=3.0.2,<5, installed: 4.0.0]
    - idna [required: >=2.5,<3, installed: 2.10]
    - urllib3 [required: >=1.21.1,<1.27, installed: 1.26.5]
jenkinsapi==0.3.11
  - pytz [required: >=2014.4, installed: 2021.1]
  - requests [required: >=2.3.0, installed: 2.25.1]
    - certifi [required: >=2017.4.17, installed: 2021.5.30]
    - chardet [required: >=3.0.2,<5, installed: 4.0.0]
    - idna [required: >=2.5,<3, installed: 2.10]
    - urllib3 [required: >=1.21.1,<1.27, installed: 1.26.5]
  - six [required: >=1.10.0, installed: 1.16.0]
ordered-set==4.0.2
pipdeptree==2.0.0
  - pip [required: >=6.0.0, installed: 21.1.2]
pipreqs==0.4.10
  - docopt [required: Any, installed: 0.6.2]
  - yarg [required: Any, installed: 0.1.9]
    - requests [required: Any, installed: 2.25.1]
      - certifi [required: >=2017.4.17, installed: 2021.5.30]
      - chardet [required: >=3.0.2,<5, installed: 4.0.0]
      - idna [required: >=2.5,<3, installed: 2.10]
      - urllib3 [required: >=1.21.1,<1.27, installed: 1.26.5]
prometheus-flask-exporter==0.18.2
  - flask [required: Any, installed: 2.0.1]
    - click [required: >=7.1.2, installed: 8.0.1]
      - importlib-metadata [required: Any, installed: 4.5.0]
        - typing-extensions [required: >=3.6.4, installed: 3.10.0.0]
        - zipp [required: >=0.5, installed: 3.4.1]
    - itsdangerous [required: >=2.0, installed: 2.0.1]
    - Jinja2 [required: >=3.0, installed: 3.0.1]
      - MarkupSafe [required: >=2.0, installed: 2.0.1]
    - Werkzeug [required: >=2.0, installed: 2.0.1]
      - dataclasses [required: Any, installed: 0.8]
  - prometheus-client [required: Any, installed: 0.11.0]
python-logstash-async==2.3.0
  - limits [required: Any, installed: 1.5.1]
    - six [required: >=1.4.1, installed: 1.16.0]
  - pylogbeat [required: Any, installed: 2.0.0]
  - requests [required: Any, installed: 2.25.1]
    - certifi [required: >=2017.4.17, installed: 2021.5.30]
    - chardet [required: >=3.0.2,<5, installed: 4.0.0]
    - idna [required: >=2.5,<3, installed: 2.10]
    - urllib3 [required: >=1.21.1,<1.27, installed: 1.26.5]
setuptools==44.1.1
waitress==2.0.0
`
### Mongodb

`
(venv) daniew@l0610066974:~/pythonWspace/flaskMongo/studentsApp> pipdeptree
Flask==2.0.2
  - click [required: >=7.1.2, installed: 8.0.3]
    - importlib-metadata [required: Any, installed: 4.8.2]
      - typing-extensions [required: >=3.6.4, installed: 4.0.0]
      - zipp [required: >=0.5, installed: 3.6.0]
  - itsdangerous [required: >=2.0, installed: 2.0.1]
  - Jinja2 [required: >=3.0, installed: 3.0.3]
    - MarkupSafe [required: >=2.0, installed: 2.0.1]
  - Werkzeug [required: >=2.0, installed: 2.0.2]
    - dataclasses [required: Any, installed: 0.8]
pipdeptree==2.2.0
  - pip [required: >=6.0.0, installed: 21.3.1]
pymongo==3.12.1
setuptools==44.1.1`