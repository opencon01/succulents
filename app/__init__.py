from flask import Flask
from prometheus_flask_exporter import PrometheusMetrics

app = Flask(__name__)

# Prometehus support
metrics  = PrometheusMetrics(app) 
#metrics.info('app_info', 'ISAM API Application routes.py', version='0.0.1')

from app import routes
