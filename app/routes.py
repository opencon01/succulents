####
# This is route file for the application  
# This is an axample to show all the imports but needs to be cleaned up
#
####

# IMPORTS #
from bson.objectid import ObjectId
from flask.helpers import url_for
from flask.wrappers import Response
from re import LOCALE
from flask import Flask, render_template, flash, redirect, request, session, abort
from app import app
from app.forms import CreateForm, LoginForm, SelectEnv, SelectServername
from app.forms import ManInvForm
from app.forms import ExecPlaybookForm
from app.forms import ExecAnsibleForm
from app.forms import JBuildJobForm
from app.forms import IsamCreateRP
from app.forms import IsamRPList
import os
import subprocess
from sys import stderr, stdin, stdout
#import requests
import time
from waitress import serve
from prometheus_flask_exporter import PrometheusMetrics
import json
from pymongo import MongoClient, response
# For FileUploads
from gridfs import GridFS, grid_file
# File upload to upload folders
import os
from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename


##########
# Variables and Configs 
###

###
# File uploads
###

UPLOAD_FOLDER = 'app/static/uploads'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):     
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

###
# WTForm secret key
###
app.config['SECRET_KEY'] = 'any secret string'

###
#Mongo Definition 
###
# define the mongodb client
client = MongoClient(host='192.168.8.101', port=27017)
# define the database to use
db = client.succulent_data
grid_fs = GridFS(db)

###
# Function to select env #
###
@app.route('/env/<funk>', methods=['GET', 'POST'])
def environment(funk):
    form = SelectEnv()
    print("Selecting the env varibale for function :" +funk)
    dropdown_list = ['select', 'lab', 'dev', 'ete', 'qa', 'prod']
    description = "Select the environment from the ENVIRONMNET dropdown list and press 'Submit' Button"
#    environment = "lab"
    #if form.submit.data == True:
    if request.method == "POST":
        #env = form.environment.data
        env = request.form["environment"]
        #return hostname(env,funk)
        return redirect(url_for("hostname", env=env, funk=funk))
    #if form.environment.data:
       #return form.environment.data
    #return environment
    return render_template('select_env.html', title='Select Environment', description=description, form=form, environment=dropdown_list)

###
# Function to select appliance/server
# This is building a lookup for selectiion
###
@app.route('/hostname/<env>/<funk>', methods=['GET', 'POST'])
def hostname(env, funk):
    form = SelectServername()
    print("Selecting the server/appliance : " +env+ ";"+funk)
    #env = env
    #global servername
    #global funk
    description = "Select the servername from the SERVERNAME dropdown list and press 'Submit' Button"
    #if form.validate_on_submit():
    if request.method == "POST":
        #servername = form.servername.data
        #return isam_crtrp2(env,servername)
        hostname = request.form["servername"]
        return redirect(url_for(funk, svr=hostname, env=env))
    try:
        dropdown_svrlist = []
        filename = path+"inv/env/"+env+"/hosts"
        f = open(filename, 'r')
        row = f.readline().rstrip()
        while row:
            print("line read :" + row)
            if (row.find('nid') > 0) or (row.find('ifm') > 0) or (row.find('isam') > 0) or (row.find('isim') > 0):
                dropdown_svrlist.append(row)
                #row = f.readline().rstrip()
            row = f.readline().rstrip()
        dropdown_svrlist = list(dict.fromkeys(dropdown_svrlist))
        print(dropdown_svrlist)
        dropdown_svrlist.sort()
        print(dropdown_svrlist)
    except Exception as e:
        return str(e)
    return render_template('select_hostname.html', title='Select Server', description=description, form=form, environment=env, servername=dropdown_svrlist)    

###
# Function to retreive creds from ansible file
###
def creds(env,svr):
    pwd = ''
    print('Variables received :' +env+' and '+svr)
    try:
        filename = path+"inv/env/"+env+"/host_vars/"+svr+'/vault.yml'
        f = open(filename, 'r')
        row = f.readline().rstrip()
        while row:
            print("line read :" + row)
            tmp = row.find('nid')
            if (row.find('ult') > 0):
                print('Loop...')
                pwd = row.rsplit(': ')[1]
            row = f.readline().rstrip()
    except Exception as e:
        return str(e)   
    return(pwd)

###
# This is just printing the endpoint of the application 
###
print("http://localhost:8100/")
print("Starting Application ....")


###
# Application Routes "/" used for logging into the app
###

# define the home page route
@app.route('/')
def hello_world():
    #return render_template("index.html")
    plants = []
    cursor = db.plantData.find()
    for record in cursor:
        plantname = record["plantname"]
        soil = record["soil"]
        water = record["water"]
        light = record["light"] 
        plants.append(record)
        print(record)
        print('--------------')
        print(plants)
        print('++++++++++++++++++++++++')
        
    return render_template("read.html", res = plants)

@app.route('/create/', methods=['GET','POST'])
def create():
    form = CreateForm()
    data = {}  
     
    if form.validate_on_submit():
        print("plant name : " + request.form['plantname'])
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
    #if request.method == "POST":
        data['plantname'] = request.form['plantname']
        data['scientificname'] = request.form['scientificname']
        data['plantmaintenance'] = request.form['plantmaintenance']
        data['plantflower'] = request.form['plantflower']
        data['soil'] = request.form['soil']
        data['light'] = request.form['light']
        data['water'] = request.form['water']
        data['plantimage'] = request.form['plantimage']
        filename = secure_filename(file.filename)
        data['plantimagename'] = secure_filename(file.filename)
        data['plantimagepath'] = "static/uploads/"
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        try:
            db.plantData.insert_one(data)
        except Exception as err:
            print(err)
            
        return redirect('/') # Return template html file

    return render_template('create.html', title='Add Plant Information', form=form) # Return template html file    


@app.route('/read/', methods=['GET'])
def read():
    #return render_template("index.html")
    plants = []
    cursor = db.plantData.find()
    for record in cursor:
        plantname = record["plantname"]
        soil = record["soil"]
        water = record["water"]
        light = record["light"] 
        plants.append(record)
        print(record)
        print('--------------')
        print(plants)
        print('++++++++++++++++++++++++')
        
    return render_template("read.html", res = plants)

@app.route('/readme/<id>')
def display(id):
    try:
        plant = db.plantData.find_one({'_id': ObjectId(id)})
        image = plant['plantimagepath'] + plant['plantimagename']
        plant['plantimagepath']
        print("Image :" + image)
    except Exception as err:
        print(err)
        return render_template("error.html", resp = err)
    return render_template("read_details.html", res = plant, image = image, title = plant['plantname'])

@app.route('/update/<id>', methods = ['GET','POST'] )
def update(id):
    form = CreateForm()
    data = {} 
    if request.method == 'POST':
        print("Posting....")
        data['plantname'] = request.form['plantname']
        data['scientificname'] = request.form['scientificname']
        data['plantmaintenance'] = request.form['plantmaintenance']
        data['plantflower'] = request.form['plantflower']
        data['soil'] = request.form['soil']
        data['light'] = request.form['light']
        data['water'] = request.form['water']
        data['plantimage'] = request.form['plantimage']
        data['plantimagename'] = request.form['plantimagename']
        data['plantimagepath'] = request.form['plantimagepath']
        try:
            db.plantData.update_one({'_id': ObjectId(id) }, {"$set": data})
        except Exception as err:
            print(err)
            return render_template("error.html", resp = err)
        
        return render_template("success.html", resp = response)
    
    try:
        plant = db.plantData.find_one({'_id': ObjectId(id)})
    except Exception as err:
        print(err)
        return render_template("error.html", resp = err)
    return render_template("update.html", form = form, res = plant, id = id)    

@app.route('/updates/<id>') # to Deleted
def update_id(id):
    try:
        plant = db.plantData.update_one({'_id': ObjectId(id)})
    except Exception as err:
        print(err)
        return render_template("error.html", resp = err)
    return render_template("index.html")

@app.route('/delete/<id>')
def delete(id):
    try:
        response = db.plantData.delete_one({'_id': ObjectId(id)})
    except Exception as err:
        print(err)
        return render_template("error.html", resp = err)
    return render_template("success.html", resp = response)

@app.route('/about/')
def about():
    return render_template("about.html")

@app.route("/oldRoot", methods=['GET', 'POST'])
# @app.route('/login/', methods=['GET' , 'POST'])
def login():

    form = LoginForm() # Get attributes from the form.py 
    app.logger.info("LogIn request received",'connection reset', extra=msg) # Log logstash MIS Data
    data_to_produce = msg
    data_to_produce['route'] = 'login'
    if form.validate_on_submit():
        flash('Login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        session['logged_in'] = True
        session['id'] = form.username.data
        app.logger.info("Redirect to Index page %s", form.username.data) # Log logstash MIS Data
        data_to_produce['username'] = form.username.data # Build Kafka Audit data
        data_to_produce['saas_msg'] = 'User was logged into the application and redirected to index ' # Build Kafka Audit data
        data_to_produce['Host'] = 'djw_laptop' # Build Kafka Audit data
        try:
         produce(json.dumps(data_to_produce), kafka_topic) # Log Kafka Audit Data
        except Exception as err:
            print(err)
        return redirect('/index/') # Return template html file
    app.logger.info("Redirect to login page",'connection reset', extra=msg)
    data_to_produce['saas_msg'] = 'User was redirected to login page' # Build Kafka Audit data
    data_to_produce['Host'] = 'djw_laptop' # Build Kafka Audit data
    data_to_produce['username'] = '' # Build Kafka Audit data
    try:
        produce(json.dumps(data_to_produce), kafka_topic) # Log Kafka Audit Data
    except Exception as err:
        print(err)
    return render_template('login.html', title='Sign In', form=form) # Return template html file

###
# Application Routes "index" main page after logging into the app
###
@app.route('/index/', methods=['GET', 'POST'])
def home():
    if not session.get('logged_in'):
        app.logger.info("Not logged in yet  - redirecting to login page")
        return login()
    else:
        app.logger.info("Logged IN -> Redirecting to index page for user %s", session.get('id'),extra=msg)
        return render_template('index.html', title='Home', username=session.get('id'))

###
# Application Routes "logout" Called when logging out of the app
###
@app.route("/logout/")
def logout():
    user_id = session.get('id')
    app.logger.info("Logout request for user : %s", user_id)
    data_to_produce = msg
    data_to_produce['route'] = 'logout'
    if not session.get('id'):
        app.logger.info('Session Username not populated Redirect to Index')
        data_to_produce['saas_msg'] = 'Session Username not populated Redirect to Index'
        try:
         produce(json.dumps(data_to_produce), kafka_topic)
        except Exception as err:
            print(err) 
        return redirect('/')
    else:
        flash('Logout requested for user : ' + session.get('id'))
        session["logged_in"] = False
        session['id'] = ""
        app.logger.info('Logout - Cheerio %s ', user_id)
        data_to_produce['username'] = user_id
        data_to_produce['saas_msg'] = 'User was logged out the application'
        data_to_produce['Host'] = 'localhost'        
        try:
         produce(json.dumps(data_to_produce), kafka_topic)
        except Exception as err:
            print(err) 
        return redirect('/')

if __name__ == "__main__":
    #app.run(debug=True , host='0.0.0.0' , port=81)
    serve(app, listen='*:8100')