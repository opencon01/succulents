from sys import setprofile
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.fields.simple import TextAreaField
#from wtforms.fields.core import SelectField
from wtforms.validators import DataRequired

class CreateForm(FlaskForm):
    plantname = StringField('PlantName', validators=[DataRequired()])
    scientificname = StringField('ScientificName', validators=None)
    plantmaintenance = TextAreaField('Plantmaintenance', validators=None) 
    plantflower = StringField('Plantflower', validators=None) 
    soil = StringField('Soil', validators=None) 
    light = StringField('Light', validators=None) 
    water = StringField('Water', validators=None) 
    plantimage = StringField('PlantImage', validators=None) 
    plantimagename = StringField('PlantImageName', validators=None) 
    plantimagepath = StringField('PlantImagePath', validators=None)    
    submit = SubmitField('Add')
    
class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class ManInvForm(FlaskForm):
    environment = StringField('Environment', validators=[DataRequired()])
    servername = StringField('ServerName', validators=[DataRequired()])
    submit = SubmitField('Submit')

class ExecPlaybookForm(FlaskForm):
    environment = StringField('Environemnt', validators=[DataRequired()])
    servername = StringField('ServerName', validators=[DataRequired()])
    playbook = StringField('Playbook', validators=[DataRequired()])
    submit = SubmitField('Submit')    

class ExecAnsibleForm(FlaskForm):
    environment = StringField('Environemnt', default='dev' , validators=[DataRequired()])
    servername = StringField('ServerName', default='zdniddb1' , validators=[DataRequired()])
    ans_module = StringField('Ansible Module', default='dd.sh' , validators=[DataRequired()])
    ans_args = StringField('Ansible Arguments', default='-a', validators=[DataRequired()])
    submit = SubmitField('Submit')    
    
class CreateStandardeReverseProxy(FlaskForm):
    environment = StringField('Environemnt', default='dev' , validators=[DataRequired()])
    appliancename = StringField('Appliance Name', default='zdniddb1' , validators=[DataRequired()])
    ans_module = StringField('Ansible Module', default='dd.sh' , validators=[DataRequired()])
    ans_args = StringField('Ansible Arguments', default='-a', validators=[DataRequired()])
    submit = SubmitField('Submit')

class JBuildJobForm(FlaskForm):
    job_name = StringField('Job Name', default='Select job_name' , validators=[DataRequired()])
    submit = SubmitField('Submit')

class IsamCreateRP(FlaskForm):
    inst_name = StringField('Instance Name', default='IamiReporting' , validators=[DataRequired()])
    host = StringField('Host Name', default='nbpisamiddev02' , validators=[DataRequired()]) 
    listening_port = StringField('Listening Port', default='7499' , validators=[DataRequired()])
    domain = StringField('Domain', default='External' , validators=[DataRequired()])
    admin_id = StringField('Admin ID', default='external' , validators=[DataRequired()])
    #admin_pwd = PasswordField('Admin Password', default='password' , validators=[DataRequired()])
    http_yn = BooleanField('Http', default='' , validators=[DataRequired()]) 
    http_port = StringField('Http Port', default='80' , validators=[DataRequired()]) 
    https_yn = BooleanField('Https', default='checked' , validators=[DataRequired()])
    https_port = StringField('Https Port', default='443' , validators=[DataRequired()]) 
    nw_interface_yn = BooleanField('Network Interface', default='checked' , validators=[DataRequired()]) 
    ip_address = StringField('IP Address', default='10.58.31.144' , validators=[DataRequired()])
    junction_point = StringField('Junction Point', default='"/ -f"' , validators=[DataRequired()])
    junction_type = StringField('Junction Type', default='"tcp"' , validators=[DataRequired()])
    server_hostname = StringField('Server Hostname', default='secautomation.africa.nedcor.net' , validators=[DataRequired()])
    server_port = StringField('Server Port', default='80' , validators=[DataRequired()])
    submit = SubmitField('Submit')   
    
class IsamRPList(FlaskForm):
    Name = StringField('Name')
    message = StringField('Message')
    
class SelectEnv(FlaskForm):
    environment = StringField('Environment', validators=[DataRequired()])
    submit = SubmitField('Submit')
    
class SelectServername(FlaskForm):
    servername = StringField('ServerName', validators=[DataRequired()])
    submit = SubmitField('Submit')