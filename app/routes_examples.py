from flask.helpers import url_for
from flask.wrappers import Response
from logstash import LOGSTASH_HOST
from re import LOCALE
from flask import Flask, render_template, flash, redirect, request, session, abort
from app import app
from app.forms import LoginForm, SelectEnv, SelectServername
from app.forms import ManInvForm
from app.forms import ExecPlaybookForm
from app.forms import ExecAnsibleForm
from app.forms import JBuildJobForm
from app.forms import IsamCreateRP
from app.forms import IsamRPList
import os
import subprocess
from sys import stderr, stdin, stdout
import jenkinsapi
from jenkinsapi.jenkins import Jenkins
import requests
import time
from waitress import serve
from logstash_async.handler import AsynchronousLogstashHandler
from logstash_async.formatter import FlaskLogstashFormatter
from prometheus_flask_exporter import PrometheusMetrics # This one can be removed as prometheus are imported in __init__.py
import logging
import json
from confluent_kafka import Consumer, Producer

#Function for sending data to Kafka Topic
def produce(data, topic):
    print("Data Received", data)
    #producer_request = Producer({'bootstrap.servers': '10.58.150.57:9092'})
    producer_request = Producer({'bootstrap.servers': app.config["BOOTSTRAP_SERVERS"]})
    print(producer_request)
    #producer_request.poll(60)
    # producer_request.produce(topic, data.encode('utf-8'))
    try:
        # producer_request.produce(topic, data)
        prod_res = producer_request.produce(topic, data.encode('utf-8'))
        print(prod_res)
        producer_request.flush()
        # print(res)
    except Exception as err:
        print(err)
 

            
#Function to select env 
@app.route('/env/<funk>', methods=['GET', 'POST'])
def environment(funk):
    form = SelectEnv()
    print("Selecting the env varibale for function :" +funk)
    dropdown_list = ['select', 'lab', 'dev', 'ete', 'qa', 'prod']
    description = "Select the environment from the ENVIRONMNET dropdown list and press 'Submit' Button"
#    environment = "lab"
    #if form.submit.data == True:
    if request.method == "POST":
        #env = form.environment.data
        env = request.form["environment"]
        #return hostname(env,funk)
        return redirect(url_for("hostname", env=env, funk=funk))
    #if form.environment.data:
       #return form.environment.data
    #return environment
    return render_template('select_env.html', title='Select Environment', description=description, form=form, environment=dropdown_list)

#Function to select appliance/server
@app.route('/hostname/<env>/<funk>', methods=['GET', 'POST'])
def hostname(env, funk):
    form = SelectServername()
    print("Selecting the server/appliance : " +env+ ";"+funk)
    #env = env
    #global servername
    #global funk
    description = "Select the servername from the SERVERNAME dropdown list and press 'Submit' Button"
    #if form.validate_on_submit():
    if request.method == "POST":
        #servername = form.servername.data
        #return isam_crtrp2(env,servername)
        hostname = request.form["servername"]
        return redirect(url_for(funk, svr=hostname, env=env))
    try:
        dropdown_svrlist = []
        filename = path+"inv/env/"+env+"/hosts"
        f = open(filename, 'r')
        row = f.readline().rstrip()
        while row:
            print("line read :" + row)
            if (row.find('nid') > 0) or (row.find('ifm') > 0) or (row.find('isam') > 0) or (row.find('isim') > 0):
                dropdown_svrlist.append(row)
                #row = f.readline().rstrip()
            row = f.readline().rstrip()
        dropdown_svrlist = list(dict.fromkeys(dropdown_svrlist))
        print(dropdown_svrlist)
        dropdown_svrlist.sort()
        print(dropdown_svrlist)
    except Exception as e:
        return str(e)
    return render_template('select_hostname.html', title='Select Server', description=description, form=form, environment=env, servername=dropdown_svrlist)    

#Function to retreive creds from ansible file
def creds(env,svr):
    pwd = ''
    print('Variables received :' +env+' and '+svr)
    try:
        filename = path+"inv/env/"+env+"/host_vars/"+svr+'/vault.yml'
        f = open(filename, 'r')
        row = f.readline().rstrip()
        while row:
            print("line read :" + row)
            tmp = row.find('nid')
            if (row.find('ult') > 0):
                print('Loop...')
                pwd = row.rsplit(': ')[1]
            row = f.readline().rstrip()
    except Exception as e:
        return str(e)   
    return(pwd)

# Message Templates
msg = {
        "ApiIdentity": "IAMI SaaS",
        "LogID": "84c08773-3122-4ee3-b086-a00a358e60f0",
        "UtcDateCreated": "2021-04-06 1:05:12 PM",
        "Name": "IAMI SAAS",
        "Host": "djw_laptop",
        "Action": "POST /v3/nedbankid/v3/users/authenticate",
        "HttpStatusCode": "200",
        "ElapsedMilliseconds": "98",
        "Header": {
            "cache-control": "no-cache",
            "connection": "Keep-Alive",
            "pragma": "no-cache",
            "via": "1.1 AQAAAN8okzQ-",
            "content-length": "120",
            "content-type": "application/json; utf-8",
            "accept": "application/json",
            "authorization": "Bearer ……………………………………………………..gjJ10hUnfw",
            "host": "djw-mynedbankid-rst.nednet.co.za",
            "user-agent": "Java/1.8.0_281",
            "x-client-ip": "10.59.115.84",
            "x-global-transaction-id": "2b5f4bf3606c5c894216e26f",
            "x-forwarded-host": "api-qa.it.nednet.co.za",
            "x-forwarded-proto": "https",
            "x-forwarded-port": "443",
            "x-forwarded-for": "10.59.115.84, 10.58.10.211",
            "ResultCode": "R00",
            "Client": "9c93843d-188d-47cd-984a-57b1871d82a3",
            "UserSession": "19b14f48-8fc0-4985-8f0c-7b50f6e9efef",
            "Nidsp": "20",
            "IsVerificationIdExists": False
        },
        "Jwt": {
            "Issuer": "http://djw-idp.nedbank.co.za",
            "Subject": "",
            "ValidFrom": "2021-04-06T13:05:13Z",
            "ValidTo": "2021-04-07T13:05:13Z",
            "Claims": {
                "sessionid": "19b14f48-8fc0-4985-8f0c-7b50f6e9efef",
                "grant_type": "anonymous",
                "token_type": "Bearer",
                "nidsp": "20",
                "iss": "http://idp.nedbank.co.za",
                "aud": "9c93843d-188d-47cd-984a-57b1871d82a3",
                "exp": "1617800713",
                "jti": "TesMxMEOO0IHRkpUw1jn5qEJq5Xb1te6",
                "iat": "1617714313",
                "nbf": "1617714313"
            }
        }
    }


#kafka_topic = 'nedbank-sys-iami-docker-test'
kafka_topic = app.config["KAFKA_TOPIC"]
reverseproxy_id=''
path = app.config["AUTOMATION_PATH"]

print("http://localhost:8100/")

# logstash configuration
app.logger.setLevel(logging.INFO)

LOGSTASH_HOST = app.config["LOGSTASH_HOST"]
LOGSTASH_DB_PATH = app.config["LOGSTASH_DB_PATH"]
LOGSTASH_TRANSPORT = app.config["LOGSTASH_TRANSPORT"]
LOGSTASH_PORT = app.config["LOGSTASH_PORT"]

# logstash handler
logstash_handler = AsynchronousLogstashHandler(
    LOGSTASH_HOST,
    LOGSTASH_PORT,
    database_path=LOGSTASH_DB_PATH,
    transport=LOGSTASH_TRANSPORT,
)

# logstash formatter
logstash_handler.formatter = FlaskLogstashFormatter(
    metadata={"beat": "iami_saas_app", "version": "0.0.3"})

# atatch handler
#app.logger.addHandler(logstash_handler)

app.logger.info("Starting iami_saas Application")


@app.route('/index/', methods=['GET', 'POST'])
def home():
    if not session.get('logged_in'):
        app.logger.info("Not logged in yet  - redirecting to login page")
        return login()
    else:
        app.logger.info("Logged IN -> Redirecting to index page for user %s", session.get('id'),extra=msg)
        return render_template('index.html', title='Home', username=session.get('id'))


@app.route("/error/<err>")
def error(err):
    app.logger.info("Oops Error Happened : %s", err)
    return render_template('error.html', title="Error Page",err=err)

@app.route("/success/")
def success():
    app.logger.info("Seccessfull Operation")
    return render_template('success.html', title="SuccessfullOperation")

@app.route('/health/', methods=['GET', 'POST'])
def health_view():
    if not session.get('logged_in'):
        # return render_template('login.html')
        return login()
    else:
        return render_template('inventory.html')


@app.route('/fixpacks/', methods=['GET', 'POST'])
def fixpacks_view():
    if not session.get('logged_in'):
        # return render_template('login.html')
        return login()
    else:
        return render_template('fixpacks.html')


@app.route('/about/')
def about():
    if not session.get('logged_in'):
        # return render_template('login.html')
        return login()
    else:
        return render_template('about.html')


@app.route('/broken/')
def broken():
    if not session.get('logged_in'):
        # return render_template('login.html')
        return login()
    else:
        return render_template('construction.html')


@app.route("/", methods=['GET', 'POST'])
# @app.route('/login/', methods=['GET' , 'POST'])
def login():

    form = LoginForm()
    app.logger.info("LogIn request received",'connection reset', extra=msg)
    data_to_produce = msg
    data_to_produce['route'] = 'login'
    if form.validate_on_submit():
        flash('Login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        session['logged_in'] = True
        session['id'] = form.username.data
        app.logger.info("Redirect to Index page %s", form.username.data)
        data_to_produce['username'] = form.username.data
        data_to_produce['saas_msg'] = 'User was logged into the application and redirected to index '
        data_to_produce['Host'] = 'djw_laptop'
        try:
         produce(json.dumps(data_to_produce), kafka_topic)
        except Exception as err:
            print(err)
        return redirect('/index/')
    app.logger.info("Redirect to login page",'connection reset', extra=msg)
    data_to_produce['saas_msg'] = 'User was redirected to login page'
    data_to_produce['Host'] = 'djw_laptop'
    data_to_produce['username'] = ''
    try:
        produce(json.dumps(data_to_produce), kafka_topic)
    except Exception as err:
        print(err)
    return render_template('login.html', title='Sign In', form=form)


@app.route("/logout/")
def logout():
    user_id = session.get('id')
    app.logger.info("Logout request for user : %s", user_id)
    data_to_produce = msg
    data_to_produce['route'] = 'logout'
    if not session.get('id'):
        app.logger.info('Session Username not populated Redirect to Index')
        data_to_produce['saas_msg'] = 'Session Username not populated Redirect to Index'
        try:
         produce(json.dumps(data_to_produce), kafka_topic)
        except Exception as err:
            print(err) 
        return redirect('/')
    else:
        flash('Logout requested for user : ' + session.get('id'))
        session["logged_in"] = False
        session['id'] = ""
        app.logger.info('Logout - Cheerio %s ', user_id)
        data_to_produce['username'] = user_id
        data_to_produce['saas_msg'] = 'User was logged out the application'
        data_to_produce['Host'] = 'localhost'        
        try:
         produce(json.dumps(data_to_produce), kafka_topic)
        except Exception as err:
            print(err) 
        return redirect('/')


@app.route("/wiki/")
def wiki():
    if not session.get('id'):
        return login()
    else:
        return redirect('http://zpifmap1/nedbank/')


@app.route("/man_inv/", methods=['GET', 'POST'])
def man_inv():
    form = ManInvForm()
    dropdown_list = ['select', 'lab', 'dev', 'ete', 'qa', 'prod']
    description = "Select the environment from the ENVIRONMNET dropdown list and press 'Submit' Button"

    #dropdown_svrlist = ['z_niddb1' , 'zdniddb1' , 'zeniddb1' , 'zqniddb1' , 'zpniddb1']
    # if form.validate_on_submit():
    if form.environment.data:
        dropdown_list = [form.environment.data]
        description = "Select the servername from the SERVERNAME dropdown list and press 'Submit' Button"

        if form.servername.data:
            dropdown_svrlist = [form.servername.data]
            description = ""

            try:
                app_path = os.path.dirname(__file__)
                #rel_path = "../automation/inv/env/"
                #path = os.path.join(app_path, rel_path)
                #path = "/opt/IBM/automation/inv/"
                #filename = path+form.environment.data+"/dev/host_vars/" + \
                filename = path+"inv/env/"+form.environment.data+"/host_vars/"+form.servername.data+"/vars.yml"  # Fix this line
                f = open(filename, 'r')
                row = f.readline().rstrip()
                while row:
                    print("line read :" + row)
                    description = "\r"+description+row
                    row = f.readline().rstrip()
            except Exception as err:
                print("Request Exception Error : " + str(err))
                return render_template('error.html', title="Error Page", err=err)
                # return str(e)

            url = "http://secautomation/reports/inv/" + \
                form.environment.data + "/" + form.servername.data + ".html"
            return render_template('man_inv.html', title='Completed', form=form, url=url, description=description, environment=dropdown_list, servername=dropdown_svrlist,  server=form.servername.data)

        #flash('Request has been submitted for server : {}'.format(form.servername.data))
        #flash('environment: {}'.format(form.environment.data))
        #flash('servername : {}'.format(form.servername.data))
        #url = "http://secautomation/reports/inv/" +  form.environment.data  + "/" +  form.servername.data  + ".html"
        #url = ""
        #flash('Request Completed. Follow link : {}'.format('<a href=url>link</a>'))
        # <a href="/man_inv/">Manual HealthChecks</a>
        # return redirect(url)
        # return redirect('/man_inv/')
        ######
        try:
            dropdown_svrlist = []
            #app_path = os.path.dirname(__file__)
            #rel_path = "../inv/env/"
            #path = os.path.join(app_path, rel_path)
            filename = path+"inv/env/"+form.environment.data+"/hosts"
            f = open(filename, 'r')
            row = f.readline().rstrip()
            while row:
                print("line read :" + row)
                if (row.find('nid') > 0) or (row.find('ifm') > 0):
                    dropdown_svrlist.append(row)
                    #row = f.readline().rstrip()
                row = f.readline().rstrip()
            dropdown_svrlist = list(dict.fromkeys(dropdown_svrlist))
        except Exception as e:
            return str(e)
        #######
        # return render_template('man_inv.html', title='Completed', form=form, url=url, environment=dropdown_list , servername=dropdown_svrlist)
        return render_template('man_inv.html', title='Select Server', description=description, form=form, environment=dropdown_list, servername=dropdown_svrlist)
    # return render_template('man_inv.html', title='Submit Manual HealthCheck', form=form, environment=dropdown_list , servername=dropdown_svrlist)
    return render_template('man_inv.html', title='Select Environment', description=description, form=form, environment=dropdown_list)


@app.route("/exec_ansible/", methods=['GET', 'POST'])
def exec_ansible():
    form = ExecAnsibleForm()
    if form.validate_on_submit:
        print('env: '+form.environment.data)
        print('server: ' + form.servername.data)
        print('ans module: ' + form.ans_module.data)
        print('ans arg : ' + form.ans_args.data)

        try:
            server = os.popen("./dd.sh")
            now_dd = server.read()
            print("Output of script : " + now_dd)
        except OSError as e:
            print('Error: ' + e)
            str(e)

    dropdown_list = ['lab', 'dev', 'ete', 'qa', 'prod']
    if form.environment.data:
        dropdown_list = [form.environment.data]

        if form.servername.data:
            dropdown_svrlist = [form.servername.data]
            description = ""

            try:
                app_path = os.path.dirname(__file__)
                rel_path = "../inv/env/"
                path = os.path.join(app_path, rel_path)
                path = "/opt/IBM/automation/inv/"
                filename = path+form.environment.data + \
                    "/host_vars/"+form.servername.data+"/vars.yml"
                f = open(filename, 'r')
                row = f.readline().rstrip()
                while row:
                    print("line read :" + row)
                    description = "\r"+description+row
                    row = f.readline().rstrip()
            except Exception as e:
                return str(e)

# Running command line test example

            p = subprocess.Popen(["ping", "-c", "3", "www.cyberciti.biz"],
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output, err = p.communicate()
            print("Output : ", output)
            now = output

#####
# Running command line test example
            cmdping = "ping -c4 www.google.com"
            p = subprocess.Popen(
                cmdping, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            while True:
                out = p.stderr.read(1)
                stdout = p.stdout.read(1)
                print('SRDERR ouput:', out)
                print('STDOUT ouput:', stdout)
                if out == '' or p.poll() != None:
                    print('first if ouput:', out)
                    break
                if out != '':
                    print('Second IF ouput:', out)

                    # sys.stdout.write(out)
                    # sys.stdout.flush()
####

            url = "http://secautomation/reports/inv/" + \
                form.environment.data + "/" + form.servername.data + ".html"
            return render_template('exec_ansible.html', title='Completed', form=form, url=url, description=description, environment=dropdown_list, servername=dropdown_svrlist,  server=form.servername.data, now_dd=now_dd, out=out, now=now)

        try:
            dropdown_svrlist = []
            app_path = os.path.dirname(__file__)
            rel_path = "../inv/env/"
            path = os.path.join(app_path, rel_path)
            filename = path+form.environment.data+"/hosts"
            f = open(filename, 'r')
            row = f.readline().rstrip()
            while row:
                print("line read :" + row)
                if (row.find('nid') > 0) or (row.find('ifm') > 0):
                    dropdown_svrlist.append(row)
                    #row = f.readline().rstrip()
                row = f.readline().rstrip()
            dropdown_svrlist = list(dict.fromkeys(dropdown_svrlist))

        except Exception as e:
            return str(e)
        return render_template('exec_ansible.html', title='Select Server', form=form, environment=dropdown_list, servername=dropdown_svrlist)

    return render_template('exec_ansible.html', title='Select Environemnt', form=form, environment=dropdown_list)


@app.route("/jinfo/")
def jinfo():
    print("Getting Jenkins Information")

    try:
        #J = Jenkins('http://jenkins3:8080/', username='admin', password='P@ssw0rd', ssl_verify=False , useCrumb=True)
        J = Jenkins('http://secautomation:8080/', username='admin',
                    password='P@ssw0rd', ssl_verify=False, useCrumb=True)
    except requests.exceptions.ConnectionError as err:
        print("Request Exception Error : " + str(err))
        return render_template('error.html', title="Error Page", err=err)

    #server = Jenkins('http://192.168.2.129:8080/', username='daniew', password='P@ssw0rd', ssl_verify=False)
    all = jenkinsapi.__all__
    print(all)

    flash("Jenkins API Version : " +
          jenkinsapi.__version__ + " and all values : ")
    session["logged_in"] = True
    session['id'] = ""
    return redirect('/index/')
    # return "Jenkins API Version : " + jenkinsapi.__version__ + " and all values : "


@app.route("/jbuild_job/", methods=['GET', 'POST'])
def jbuild_job():
    print("Jenkins Bulding Jobs")

# Define the form to be used
    form = JBuildJobForm()

# Defining all Variables
    auth = '11197564537657b7d6436186bdc269f933'
    jenkins_url = 'http://jenkins3:8080'
    result = "Failed"
    build_number = 0
    dropdown_job_list = []

# Defining Methods
    def get_server_instance():

        # auth='116961a1ab33c97f7f28133f5521374fd5'
        #jenkins_url = 'http://192.168.2.129:8080'
        #server = Jenkins(jenkins_url, username='daniew', password=auth , ssl_verify=False, useCrumb=True)
        try:
            #server = Jenkins('http://jenkins3:8080/', username='admin', password='P@ssw0rd', ssl_verify=False, useCrumb=True)
            server = Jenkins('http://secautomation:8080/', username='admin',
                             password='P@ssw0rd', ssl_verify=False, useCrumb=True)

        except requests.exceptions.ConnectionError as err:
            print("Request Exception Error : " + str(err))
            return render_template('error.html', title="Error Page", err=err)
        return server

    # Gtting crumb data
    #server = get_server_instance()

    #crumb_data = requests.get("http://192.168.2.129:8080/crumbIssuer/api/python", username='daniew', auth=auth).python()
    # crumb_data = requests.get(jenkins_url"/crumbIssuer/api/python").python()
    #headers = {'Jenkins-Crumb': crumb_data['crumb']}
    #crumb_data = requests.get('http://192.168.2.129:8080/crumbIssuer/api/python/', username='daniew', password='P@ssw0rd', ssl_verify=False)
    #headers = {'Jenkins-Crumb': crumb_data['crumb']}

    # looking up jobs
    print("Getting Jobs ...")

    #job_name = "002"
    if form.validate_on_submit():
        job_name = form.job_name.data
        print("Job Name : " + job_name)
        server = get_server_instance()
        # if (server.has_job(job_name)):
        #   job_instance = server.get_job(job_name)
        #   last_build = job_instance.get_last_build()
        #   if (job_instance.is_enabled() == True):
        #      job_instance.disable()
        #      result  = "Disabled"
        #   else:
        #      job_instance.enable()
        #      result  = "Enabled"
        #   flash("Done Disabling/Enabling Job name: " + job_name + " | Result :" +result )
        #   return redirect('/')

        # if (job_instance.is_enabled() == True):
        # if server.has_job(job_name): #Coomented this out and changed the format
        dd = server.get_job(job_name)
        print('server Info : ' + server.version + "," +
              str(server.has_job(job_name)))  # djw
        job_instance = server.get_job(job_name)
        queue_item = job_instance.invoke()
        print("building ....")
        queue_item.block_until_complete()
        build = job_instance.get_last_build()
        print(" Last Buld number was : " + str(build.get_number()))
        print("Build status was : " + build.get_status())
        build_number = build.get_number()
        result = build.get_status()
        #print(" Last Buld number was : " + build.get_revision_branch())
        flash("Done Building Job name: " + job_name + " | Result :" + result + " and the build number was :" +
              str(build_number) + " http://jenkins3:8080/job/"+job_name+"/" + str(build_number)+"/")
        session["logged_in"] = True
        session['id'] = ""
        return redirect('/index/')

    server = get_server_instance()
    # try:
    for job_name, job_instance in server.get_jobs():
        print('Job Name:%s' % (job_instance.name))
        print('Job Description:%s' % (job_instance.get_description()))
        print('Is Job running:%s' % (job_instance.is_running()))
        print('Is Job enabled:%s' % (job_instance.is_enabled()))
        dropdown_job_list.append(job_instance.name)
#   except expression as identifier:
#      print("Job_name Exception Error : " + str(err))
#      return render_template('error.html', title="Error Page", err=err)
    return render_template('jbuild_job.html', title="Select the Jobs to Build", description="Select the Job from the Jenkins server that you would like to build", form=form, job_name=dropdown_job_list)

# START :  TO BE DELETED SECTION ------
# This section was part of my trial and error but can still be usefull

    jenkins_url = "http://secautomation:8080"
    auth = ('daniew', 'P@ssw0rd')
    job_name = "002"
    #request_url = "{0:s}/job/{1:s}/buildWithParameters".format(jenkins_url,job_name,)
    request_url = "{0:s}/job/{1:s}".format(jenkins_url, job_name,)

    print("Determining next build number")
    job = requests.get(
        "{0:s}/job/{1:s}/api/json".format(
            jenkins_url,
            job_name,
        ),
        auth=auth,
    ).json()
    next_build_number = job['nextBuildNumber']
    next_build_url = "{0:s}/job/{1:s}/{2:d}/api/json".format(
        jenkins_url,
        job_name,
        next_build_number,
    )

    params = {"Foo": "String param 1", "Bar": "String param 2"}
    print("Triggering build: {0:s} #{1:d}".format(job_name, next_build_number))
    response = requests.post(
        request_url,
        data=params,
        auth=auth,
        headers=headers,
    )

    response.raise_for_status()
    print("Job triggered successfully")

    while True:
        print("Querying Job current status...")
        try:
            build_data = requests.get(next_build_url, auth=auth).json()
        except ValueError:
            print("No data, build still in queue")
            print("Sleep for 20 sec")
            time.sleep(20)
            continue

        print("Building: {0}".format(build_data['building']))
        building = build_data['building']
        if building is False:
            break
        else:
            print("Sleep for 60 sec")
            time.sleep(60)

    print("Job finished with status: {0:s}".format(build_data['result']))
    return "Job finished with status: {0:s}".format(build_data['result'])

# END : TO BE DELETED SECTION ------

################################


@app.route("/jdisable_job/", methods=['GET', 'POST'])
def jdisable_job():

    # Define the form to be used
    form = JBuildJobForm()

# Defining all Variables
    auth = '11197564537657b7d6436186bdc269f933'
    jenkins_url = 'http://jenkins3:8080'
    result = "Failed"
    build_number = 0
    dropdown_job_list = []

# Defining Methods
    def get_server_instance():

        # auth='116961a1ab33c97f7f28133f5521374fd5'
        #jenkins_url = 'http://192.168.2.129:8080'
        #server = Jenkins(jenkins_url, username='daniew', password=auth , ssl_verify=False)
        server = Jenkins('http://secautomation:8080/', username='admin',
                         password='P@ssw0rd', ssl_verify=False, useCrumb=True)
        return server

    # Gtting crumb data
    server = get_server_instance()

    #crumb_data = requests.get("http://192.168.2.129:8080/crumbIssuer/api/python", username='daniew', auth=auth).python()
    # crumb_data = requests.get(jenkins_url"/crumbIssuer/api/python").python()
    #headers = {'Jenkins-Crumb': crumb_data['crumb']}
    #crumb_data = requests.get('http://192.168.2.129:8080/crumbIssuer/api/python/', username='daniew', password='P@ssw0rd', ssl_verify=False)
    #headers = {'Jenkins-Crumb': crumb_data['crumb']}

    # looking up jobs
    print("Getting Jobs ...")

    for job_name, job_instance in server.get_jobs():
        print('Job Name:%s' % (job_instance.name))
        print('Job Description:%s' % (job_instance.get_description()))
        print('Is Job running:%s' % (job_instance.is_running()))
        print('Is Job enabled:%s' % (job_instance.is_enabled()))
        dropdown_job_list.append(job_instance.name)

    #job_name = "002"
    if form.validate_on_submit():
        job_name = form.job_name.data
        print("Job Name : " + job_name)

        if (server.has_job(job_name)):
            job_instance = server.get_job(job_name)
            last_build = job_instance.get_last_build()
            if (job_instance.is_enabled() == True):
                job_instance.disable()
                result = "Disabled"
            flash("Done Disabling Job name: " +
                  job_name + " | Result :" + result)
            session["logged_in"] = True
            session['id'] = ""
            return redirect('/index/')

    return render_template('jbuild_job.html', title="Select Job to Disable", description="Select the Job from the Jenkins server that you would like to disable", form=form, job_name=dropdown_job_list)


@app.route("/jenable_job/", methods=['GET', 'POST'])
def jenable_job():

    # Define the form to be used
    form = JBuildJobForm()

    # Defining all Variables
    auth = '11197564537657b7d6436186bdc269f933'
    jenkins_url = 'http://jenkins3:8080'
    result = "Failed"
    build_number = 0
    dropdown_job_list = []

    # Defining Methods
    def get_server_instance():

        # auth='116961a1ab33c97f7f28133f5521374fd5'
        #jenkins_url = 'http://192.168.2.129:8080'
        #server = Jenkins(jenkins_url, username='daniew', password=auth , ssl_verify=False)
        server = Jenkins('http://secautomation:8080/', username='admin',
                         password='P@ssw0rd', ssl_verify=False, useCrumb=True)
        #J = Jenkins('http://jenkins3:8080/', username='admin', password='P@ssw0rd', ssl_verify=False , useCrumb=True)
        return server

    # Gtting crumb data
    server = get_server_instance()

    #crumb_data = requests.get("http://192.168.2.129:8080/crumbIssuer/api/python", username='daniew', auth=auth).python()
    # crumb_data = requests.get(jenkins_url"/crumbIssuer/api/python").python()
    #headers = {'Jenkins-Crumb': crumb_data['crumb']}
    #crumb_data = requests.get('http://192.168.2.129:8080/crumbIssuer/api/python/', username='daniew', password='P@ssw0rd', ssl_verify=False)
    #headers = {'Jenkins-Crumb': crumb_data['crumb']}

    # looking up jobs
    print("Getting Jobs ...")

    for job_name, job_instance in server.get_jobs():
        print('Job Name:%s' % (job_instance.name))
        print('Job Description:%s' % (job_instance.get_description()))
        print('Is Job running:%s' % (job_instance.is_running()))
        print('Is Job enabled:%s' % (job_instance.is_enabled()))
        dropdown_job_list.append(job_instance.name)

    #job_name = "002"
    if form.validate_on_submit():
        job_name = form.job_name.data
        print("Job Name : " + job_name)

        if (server.has_job(job_name)):
            job_instance = server.get_job(job_name)
            last_build = job_instance.get_last_build()
            if (job_instance.is_enabled() == False):
                job_instance.enable()
                result = "Enabled"
            flash("Done Enabling Job name: " +
                  job_name + " | Result :" + result)
            session["logged_in"] = True
            session['id'] = ""
            return redirect('/index/')

    return render_template('jbuild_job.html', title="Select Job to Enable", description="Select the Job from the Jenkins server that you would like to enable", form=form, job_name=dropdown_job_list)

@app.route("/isam_crtrp/", methods=['GET', 'POST'])
def isam_crtrp():
    return environment('isam_crtrp2')
    
@app.route("/isam_crtrp2/<env>/<svr>", methods=['GET', 'POST'])
def isam_crtrp2(env, svr):
    #####
    # Things taht needs to be added
    # Reading instance /Updating instances and more specifically adding/Read/Update junctions
    # Validation of what are submitted like available ports and ips and junction configs
    # adding more junctions to an instance
    # Adding all possible fields as per ibmsecurity layer to cater for all type -> kerberos as an example
    #
    # Add elk logging
    #
    # Add Real logginto into saas or protect it with own webseal using AD authN
    #####
 
    
    form = IsamCreateRP()
    domain_list = ['Default', 'external', 'imaging', 'treasury']
    admin_id_list = ['sec_master', 'external',
                     'imaging_admin', 'treasury_admin']
    form.host.data = svr
    #form.submit.data = False
    
   # if not session.get('logged_in'):
   #    #return render_template('login.html')
   #    return login()
   # else:
    # if form.validate_on_submit:
    # if form.validate_on_submit():
    #if form.submit.data == True:
    if request.method == "POST":
        spce = "    - "
        space = "      "
        try:
            app_path = os.path.dirname(__file__)
            #rel_path = "../inv/env/"
            rel_path = "../automation/isam/inv/env/"
            path = os.path.join(app_path, rel_path)
            path = "/opt/IBM/automation/inv/env/"
            filename = path+env+"/host_vars/"+svr+"/vars.yml"
            f = open(filename, 'a')
            f.write("\n#===Automated Creation for : " +
                    form.inst_name.data + "=======\n")
            f.write(spce+"inst_name:\t"+form.inst_name.data+"\n")
            f.write(space+"host:\t\t"+form.host.data+"\n")
            f.write(space+"listening_port:\t"+form.listening_port.data+"\n")
            f.write(space+"domain:\t\t"+form.domain.data+"\n")
            f.write(space+"admin_id:\t\t"+form.admin_id.data+"\n")
            f.write(space+"admin_pwd:\t{{"+form.admin_id.data+"_password }}\n")
            f.write(space+"http_yn:\t\t"+str(form.http_yn.data)+"\n")
            f.write(space+"http_port:\t"+form.http_port.data+"\n")
            f.write(space+"https_yn:\t\t"+str(form.https_yn.data)+"\n")
            f.write(space+"https_port:\t"+form.https_port.data+"\n")
            f.write(space+"nw_interface_yn:\t" +
                    str(form.nw_interface_yn.data)+"\n")
            f.write(space+"ip_address:\t"+form.ip_address.data+"\n")
            # f.write("\n###  Instance Configuration : \n")
            # f.write(space+"instances:\n")
            #f.write(spce+"inst_name: "+ form.inst_name.data+"\n")
            f.write(space+"entries:\n")
            f.write(
                space+space+'- { method: set, stanza: logging, entry_name: agent, value: "yes" }\n')
            f.write(
                space+space+'- { method: set, stanza: aznapi-configuration, entry_name: logaudit, value: "yes" }\n')
            f.write(
                space+space+'- { method: add, stanza: aznapi-configuration, entry_name: auditcfg, value: "azn" }\n')
            f.write(
                space+space+'- { method: add, stanza: aznapi-configuration, entry_name: auditcfg, value: "authn" }\n')
            f.write(
                space+space+'- { method: add, stanza: aznapi-configuration, entry_name: auditcfg, value: "http" }\n')
            f.write(space+space+'- { method: add, stanza: aznapi-configuration, entry_name: logcfg, value: "audit.authn:rsyslog server={{ add_system_alerts_rsyslog_collector }},port={{ add_system_alerts_rsyslog_collectorPort }},log_id='+form.inst_name.data+'-RPAuthN" }\n')
            f.write(space+space+'- { method: add, stanza: aznapi-configuration, entry_name: logcfg, value: "audit.azn:rsyslog server={{ add_system_alerts_rsyslog_collector }},port={{ add_system_alerts_rsyslog_collectorPort }},log_id='+form.inst_name.data+'-RPAuthZ" }\n')
            f.write(space+space+'- { method: add, stanza: aznapi-configuration, entry_name: logcfg, value: "audit.mgmt:rsyslog server={{ add_system_alerts_rsyslog_collector }},port={{ add_system_alerts_rsyslog_collectorPort }},log_id='+form.inst_name.data+'-RPMgmt" }\n')
            f.write(space+space+'- { method: add, stanza: aznapi-configuration, entry_name: logcfg, value: "http.ref:rsyslog server={{ add_system_alerts_rsyslog_collector }},port={{ add_system_alerts_rsyslog_collectorPort }},log_id='+form.inst_name.data+'-RPHttpRef" }\n')
            f.write(space+space+'- { method: add, stanza: aznapi-configuration, entry_name: logcfg, value: "http.ref:rsyslog server={{ add_system_alerts_rsyslog_collector }},port={{ add_system_alerts_rsyslog_collectorPort }},log_id='+form.inst_name.data+'-RPHttpClf" }\n')
            # f.write("\n#  Juncttion Configuration : \n")
            #f.write(spce+"inst_name: "+ form.inst_name.data+"\n")
            f.write(space+"junctions:\n")
            f.write(space+space+'- junction_point:\t' +
                    form.junction_point.data + '\n')
            f.write(space+space+'  junction_type:\t' +
                    form.junction_type.data + '\n')
            f.write(space+space+'  servers:\n')
            f.write(space+space+space+' - server_hostname:\t' +
                    form.server_hostname.data + '\n')
            f.write(space+space+space+'   server_port:\t' +
                    form.server_port.data + '\n')
            f.write("\n#===Done Automated Creation for : " +
                    form.inst_name.data + "=======\n")
            f.close()
            flash("Done created Reverse Proxy config: " + form.inst_name.data)
            session["logged_in"] = True
            #session['id'] = ""

            data_to_produce = msg
            data_to_produce['route'] = 'isam_crtrp2'
            data_to_produce['username'] = session['id']
            data_to_produce['saas_msg'] = 'User created a WebSeal Instance '
            data_to_produce['Appliance'] = svr
            data_to_produce['Instance'] = form.inst_name.data
            try:
                produce(json.dumps(data_to_produce), kafka_topic)
            except Exception as err:
                print(err)
                return error(err)

            return redirect('/success/')
        except OSError as err:
            flash("Failed to create Reverse Proxy config: " + str(err))
            session["logged_in"] = True
            session['id'] = ""
            #return redirect('/index/')
            print("Request Exception Error : " + str(err))
            return render_template('error.html', title="Error Page", err=err)
    return render_template('isam_crtRP.html', title="Create Reverse Proxy", description="Complete section and press 'Submit' to create the Reverse Proxy", form=form, domain=domain_list, admin_id=admin_id_list,env=env,svr=svr)

@app.route("/isam_viewcfg/", methods=['GET', 'POST'])
def isam_viewcfg():
    return environment('isam_viewcfg2')
    
@app.route("/isam_viewcfg2/<env>/<svr>", methods=['GET', 'POST'])
def isam_viewcfg2(env, svr):
    print("Selecting the env/svr : " +env+ " : " +svr)
    description = "View Ansible config"
    try:
        dropdown_svrlist = []
        filename = path+"inv/env/"+env+"/host_vars/"+svr+"/vars.yml"
        f = open(filename, 'r')
        contents = f.read()
    except Exception as e:
        return str(e)
    return render_template('isam_view_cfg.html', title='View Ansible config', target="_blank", description=description,text=contents,env=env,svr=svr,f=dropdown_svrlist)    
    #return Response(contents,mimetype='text/plain')


@app.route('/isam_time_cfg/', methods=['GET', 'POST'])
def isam_time_cfg():
    return environment('isam_time_cfg2')

@app.route('/isam_time_cfg2/<env>/<svr>', methods=['GET', 'POST'])
def isam_time_cfg2(env,svr):
    pwd = creds(env, svr)
    data_to_produce = msg
    data_to_produce['route'] = 'isam_time_cfg'
    try:
        produce(json.dumps(data_to_produce), kafka_topic)
    except Exception as err:
        print(err)
        return error(err)

    # ISAM Rest Service
    url = 'https://'+svr+'/time_cfg'
    headers = {
        "Accept": "application/json",
        "Authorization": ""
    }
    # looger
    app.logger.info("Using the isam_time_cfg route")
    # flask.request.headers("Accept")
    app.logger.info(
        "%s is making REST call to /<appliance>/time_cfg", session.get('id'))
    try:
        res = requests.get(url, headers=headers, auth=(
        'admin@local', pwd), verify=False)
        app.logger.info(
        "Completed the REST call to <aapliance>/time_cfg that was done by %s", session.get('id'))
        print(res.status_code, " - ", res.content)
        print(res)
        app.logger.info("Return Render the result for user : %s",
                    session.get('id'))
        # return res.json()
    except Exception as err:
        print(err)
        return error(err)
    return render_template('time_cfg.html', title="Time CFG", jsonfile=res.json(),env=env,svr=svr)

@app.route('/isam_snmp_cfg/', methods=['GET', 'POST'])
def isam_snmp_cfg():
    return environment('isam_snmp_cfg2')

@app.route('/isam_snmp_cfg2/<env>/<svr>', methods=['GET', 'POST'])
def isam_snmp_cfg2(env, svr):
    pwd = creds(env, svr)
    url = 'https://'+svr+'/snmp/v1'
    headers = {
        "Accept": "application/json",
        "Authorization": ""
    }
    # flask.request.headers("Accept")
    res = requests.get(url, headers=headers, auth=(
        'admin@local', pwd), verify=False)
    print(res.status_code, " - ", res.content)
    # return res.json()
    return render_template('snmp.html', title="SNMP", jsonfile=res.json(),env=env,svr=svr)


@app.route('/isam_snapshot_list/', methods=['GET', 'POST'])
def isam_snapshot_list():
    return environment('isam_snapshot_list2')

@app.route('/isam_snapshot_list2/<env>/<svr>', methods=['GET', 'POST'])
def isam_snapshot_list2(env,svr):
    pwd = creds(env, svr)
    url = 'https://'+svr+'/snapshots'
    headers = {
        "Accept": "application/json",
        "Authorization": ""
    }
    # flask.request.headers("Accept")
    res = requests.get(url, headers=headers, auth=(
        'admin@local', pwd), verify=False)
    print(res.status_code, " - ", res.content)
    # return jsonify(res.json())
    return render_template('3.html', title="Snapshots", jsonfile=res.json(),env=env,svr=svr)

@app.route('/isam_rp_list/', methods=['GET', 'POST'])
def isam_rp_list():
    return environment('isam_rp_list2')

@app.route('/isam_rp_list2/<env>/<svr>', methods=['GET', 'POST'])
def isam_rp_list2(env, svr):
    form = IsamRPList()
    pwd = creds(env, svr)
    url = 'https://'+svr+'/wga/reverseproxy/'
    headers = {
        "Accept": "application/json",
        "Authorization": ""
    }
    # flask.request.headers("Accept")
    #res = requests.get(url, headers=headers, auth=('admin@local', 'P@ssw0rd'), verify=False)
    try:
        res = requests.get(url, headers=headers, auth=('admin@local', pwd), verify=False)
    except Exception as err:
        return error(err)
        
    print(res.status_code, " - ", res.content)
    print(res)
    resj = json.dumps(res.json())
    print('res dumps :->', json.dumps(resj))  # to be delete used as a test

    return render_template('rp_list.html', title="RP List", jsonfile=res.json(),env=env,svr=svr)


@app.route('/isam_rp_list_stanza/<env>/<svr>/<rpid>', methods=['GET', 'POST'])
def isam_rp_list_stanza(env,svr,rpid):
    form = IsamRPList()
    print('ReverseProxy ID :', rpid)
    reverseproxy_id=rpid
    pwd = creds(env, svr)
    url = 'https://'+svr+'/wga/reverseproxy/'+rpid+'/configuration/stanza'
    headers = {
        "Accept": "application/json",
        "Authorization": ""
    }
    # flask.request.headers("Accept")
    res = requests.get(url, headers=headers, auth=(
        'admin@local', pwd), verify=False)

    print(res.status_code, " - ", res.content)
    print(res)
    resj = json.dumps(res.json())
    print('res dumps :->', json.dumps(resj))  # to be delete used as a test

    return render_template('3_1.html', title="RP Stanza List", jsonfile=res.json(), rpid=rpid,env=env,svr=svr)

@app.route('/isam_rp_list_stanza_detail/<env>/<svr>/<rpid>/<stanza_id>', methods=['GET', 'POST'])
def isam_rp_list_stanza_detail(env,svr,rpid,stanza_id):
    form = IsamRPList()
    print('Stanza ID :', stanza_id)
    print('RPid:' ,rpid)
    pwd = creds(env, svr)
    url = 'https://'+svr+'/wga/reverseproxy/'+rpid+'/configuration/stanza/'+stanza_id
    headers = {
        "Accept": "application/json",
        "Authorization": ""
    }
    # flask.request.headers("Accept")
    res = requests.get(url, headers=headers, auth=(
        'admin@local', pwd), verify=False)

    print(res.status_code, " - ", res.content)
    print(res)
    
    #return(res.json())
    return render_template('3_2.html', title="RP Stanza Detail", jsonfile=res.json(), rpid=rpid, stanza_id=stanza_id,env=env,svr=svr)



if __name__ == "__main__":
    #app.run(debug=True , host='0.0.0.0' , port=81)
    serve(app, listen='*:8200')
