####
# This is our application name 
# User to start application and if build as docker container
#
####
from app import app # Our App import 
from waitress import serve # Webserver import 

serve(app, listen='*:8100') # waitress is going to listen on this port 