# import necessary modules
from flask import Flask, request, render_template
from pymongo import MongoClient

# define the mongodb client
client = MongoClient(host='192.168.8.101', port=27017)
# define the database to use
db = client.student_data
appdb = client.user_data

# define the flask app
app = Flask(__name__)


# define the home page route
@app.route('/')
def hello_world():
    return render_template("index.html")

# route to get data from html form and insert data into database
@app.route('/data', methods=["GET", "POST"])
def data():
    data = {}
    if request.method == "POST":
        data['Name'] = request.form['name']
        data['Email'] = request.form['email']
        data['Age'] = request.form['age']
        data['DOB'] = request.form['dob']
        data['Department'] = request.form['department']
        data['Gender'] = request.form['gender']
        data['Address'] = request.form['address']
        data['Pincode'] = request.form['pincode']
        lang = []
        for i in "12345678":
            try:
                if request.form['language' + i] != "":
                    lang.append(request.form['language' + i])
            except Exception as e:
                pass
        data['Language'] = lang
        db.studentData.insert_one(data)

    return render_template("index.html")

@app.route("/read")
def read():
    names = []
    cursor = db.studentData.find()
    for record in cursor:
        name = record["Name"]
        email= record["Email"]
        department = record["Department"]
        allData = name +" , "+ email +" , "+ department 
        names.append(allData)
        #names.append(record)
        print(record)
    return render_template("response.html", res = names)

# route to get data from html form and insert data into database
@app.route('/register', methods=["GET", "POST"])
def register():
    data = {}
    if request.method == "POST":
        data['Name'] = request.form['uname']
        data['Password'] = request.form['passwd']
        data['Role'] = request.form['role']
        appdb.userData.insert_one(data)

    return render_template("register.html")    

@app.route("/showadmins")
def showadmins():
    names = []
    cursor = appdb.userData.find()
    for record in cursor:
        name = record["Name"]
        role= record["Role"]
        all = name +" , "+ role
        names.append(all)
        #names.append(record)
        print(record)
    return render_template("response.html", res = names)

if __name__ == '__main__':
    app.run(debug=False)
