ansible==4.0.0
ansible-core==2.11.1
appdirs==1.4.4
autopep8==1.5.7
certifi==2021.5.30
cffi==1.14.5
chardet==4.0.0
click==8.0.3
confluent-kafka==1.7.0
cryptography==3.4.7
dataclasses==0.8
docopt==0.6.2
Flask==2.0.1
Flask-WTF==0.15.1
ibmsecurity==2021.5.10.1
idna==2.10
importlib-metadata==4.5.0
itsdangerous==2.0.1
Jinja2==3.0.1
limits==1.5.1
MarkupSafe==2.0.1
ordered-set==4.0.2
packaging==20.9
pipdeptree==2.0.0
pipreqs==0.4.10
prometheus-client==0.11.0
prometheus-flask-exporter==0.18.2
pycodestyle==2.7.0
pycparser==2.20
pylogbeat==2.0.0
pyparsing==2.4.7
python-logstash-async==2.3.0
pytz==2021.1
PyYAML==5.4.1
requests==2.25.1
resolvelib==0.5.4
six==1.16.0
toml==0.10.2
typing-extensions==3.10.0.0
urllib3==1.26.5
waitress==2.0.0
Werkzeug==2.0.1
WTForms==2.3.3
yarg==0.1.9
zipp==3.4.1
# Mongo
dataclasses==0.8
Flask==2.0.2 #Flask
importlib-metadata==4.8.2
itsdangerous==2.0.1 #Flask
Jinja2==3.0.3 #Flask
MarkupSafe==2.0.1
pipdeptree==2.2.0
pymongo==3.12.1
typing_extensions==4.0.0
Werkzeug==2.0.2
WTForms==2.3.3 #Flask Forms
zipp==3.6.0
confluent-kafka==1.7.0 #Kafka 
python-logstash-async #Logstash
prometheus-flask-exporter==0.18.2 #Prometheus

